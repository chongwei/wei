#include<iostream>
#include<vector>
#include<algorithm>
#include<iomanip>
#define sq(x) (x)
#define tune 7 

using namespace std;
int flg=0,iter;

long long step(int n){
    if(n==1)return 1;
    else return n*step(n-1);
}
struct node{
    long long id,lb,ct,fg,gid,clu,dnb,fac,cnb;
    //vector<node*> en;
    vector<int> ei;
    vector<long long> hs;
    void aid(int d){
        ei.push_back(d);
    }
    void res(){lb=ct=0;}
    void clear(){
        fg = 0;
        cnb = 0;
        fac = 0;
        clu = -1;
        dnb = 0;
        ei.clear();
        hs.clear();
    }
    void print(){
        cout<<"id: "<<setw(4)<<id;
        for(int i=0;i<hs.size();++i)
            cout<<setw(7)<<hs[i]<<" ";
        cout<<endl;
    }
};

bool operator<(node x,node y){
    //if(x.cnb!=y.cnb)return x.cnb<y.cnb;
    if(x.dnb!=y.dnb)return x.dnb<y.dnb;
    if(x.clu!=y.clu)return x.clu<y.clu;
    //if(x.fac!=y.fac)return x.fac<y.fac;
    for(int i=0;i<x.hs.size();++i){
        if(x.hs[i]!=y.hs[i]){
            return  x.hs[i]<y.hs[i];
        }
    }
    return false;
}
bool ptrless(node* x,node*y){
    return (*x)<(*y);
}
bool operator==(node x,node y){
    for(int i=0;i<x.hs.size();++i)
        if(x.hs[i]!=y.hs[i])
            return false;
    return true;
}
bool comp(node x,node y){
    return x.id<y.id;
}

bool ptrcomp(node* x,node* y){
    return x->id<y->id;
}

node x[20000],v[20000];
node* xx[20000];
node* vv[20000];
int n,m,tp[500000],T;
int sf[20000],is[20000];
int y[1000][3];
int nxt;
int s;
void tell(node*r,node* n,int m){
    n->clu = m;
    for(int i=0;i<n->ei.size();++i)
        if(r[n->ei[i]].clu<0)
            tell(r,r+(n->ei[i]),m);
}
bool correct(int bg=1,int ed=n+1){
    ++flg;
    for(int i=bg,idx;i<ed;++i){
        for(int j=0;j<xx[i]->ei.size();++j){
            idx = sf[xx[i]->ei[j]];
            if(vv[idx]->fg!=flg){
                vv[idx]->fg = flg;
                vv[idx]->res();
            }
            if(vv[idx]->ct==0){
                vv[idx]->lb = i;
            }
            else if(vv[idx]->lb!=i&&vv[idx]->ct!=0){
                return false;
            }
            vv[idx]->ct += 1;
        }
        for(int j=0;j<vv[i]->ei.size();++j){
            idx = is[vv[i]->ei[j]];
            if(vv[idx]->fg!=flg || vv[idx]->lb != i || vv[idx]->ct==0)
                return false;
            
            else vv[idx]->ct -= 1;
        }
    }
    return true;
}
bool notsame(){
    for(int i=y[s][0];i<y[s][1]-1;++i){
        for(int j=0;j<xx[i]->ei.size();++j)
            if(xx[i]->ei[j]!=xx[i+1]->ei[j])
                return true;
    }
    return false;
}
void decide(int bg=1,int ed=n+1){
    s=0;
    y[s][0] = y[s][1] = 1;
    for(int i=bg;i<ed;++i){
        if(i!=n&&x[i]==x[i+1])
            y[s][1] = i+2;
        else{
            if(y[s][0]!=y[s][1]&&notsame()){
                y[s][2]=step(y[s][1]-y[s][0]);
                ++s;
            }
            y[s][0] = y[s][1] = i+1;
        }
    }
}

void ptrdec(int bg=1,int ed=n+1){
    s=0;
    y[s][0] = y[s][1] = 1;
    for(int i=bg;i<ed;++i){
        if(i!=n&&(*(xx[i]))==(*(xx[i+1])))
            y[s][1] = i+2;
        else{
            if(y[s][0]!=y[s][1]&&notsame()){
                y[s][2]=step(y[s][1]-y[s][0]);
                ++s;
            }
            y[s][0] = y[s][1] = i+1;
        }
    }
}
void vabelcir(int i){
    int j=i,p=0,t,m;
    bool lb=false;
    while(1){
        if(v[j].ei.size()!=2)
            return;
        if(j==v[j].ei[0]||j==v[j].ei[1])
            return;
        if(lb){
            v[j].hs[tune-1]-=m;
            ++m;
        }
        t = j;
        j = v[j].ei[(v[j].ei[0]==p)?1:0];
        p = t;
        if(j==i){
            if(lb)return;
            else{
                lb = true;
                m = 1;
            }
        }
    }
}

void xabelcir(int i){
    int j=i,p=0,t,m;
    bool lb=false;
    while(1){
        if(x[j].ei.size()!=2)
            return;
        if(j==x[j].ei[0]||j==x[j].ei[1])
            return;
        if(lb){
            x[j].hs[tune-1]-=m;
            ++m;
        }
        t = j;
        j = x[j].ei[(x[j].ei[0]==p)?1:0];
        p = t;
        if(j==i){
            if(lb)return;
            else{
                lb = true;
                m = 1;
            }
        }
    }
}

int main(){
    cin.tie(NULL);
    ios_base::sync_with_stdio(false); 
    long long idx;
    bool mm;
    cin>>T;
    clock_t start, stop;
    for(int l=0;l<T;++l){
        cin>>n>>m;
        for(int i=0;i<2*m;++i){
            cin>>tp[2*i]>>tp[2*i+1];
        }

        for(int i=1;i<=n;++i){
            sf[i] = i;
            v[i].clear();
            x[i].clear();
            xx[i] = x+i;
            vv[i] = v+i;
        }
        //x = new node[n+1];
        //v = new node[n+1];
        for(int i=0;i<m;++i){
            x[tp[2*i+1]].aid(tp[2*i]);
            x[tp[2*i]].aid(tp[2*i+1]);
        }
        for(int i=m;i<2*m;++i){
            v[tp[2*i+1]].aid(tp[2*i]);
            v[tp[2*i]].aid(tp[2*i+1]);
        }
        for(int i=1;i<=n;++i){
            //sort(x[i].ei.begin(),x[i].ei.end());
            //sort(v[i].ei.begin(),x[i].ei.end());
            x[i].hs.push_back(x[i].ei.size());
            x[i].id = i;
            v[i].hs.push_back(v[i].ei.size());
            v[i].id = i;
        }
        for(int k=0;k<tune;++k){
            for(int i=1;i<=n;++i){
                idx = 0;
                for(int j=0;j<x[i].ei.size();++j){
                    idx += sq(x[x[i].ei[j]].hs[k]);
                }
                x[i].hs.push_back(idx);
                idx = 0;
                for(int j=0;j<v[i].ei.size();++j){
                    idx += sq(v[v[i].ei[j]].hs[k]);
                }
                v[i].hs.push_back(idx);
            }
        }
       /* 
        */
//        decide(); 

        for(int i=1;i<=n;++i){
            if(x[i].clu<0)
                tell(x,x+i,i);
            if(v[i].clu<0)
                tell(v,v+i,i);
        }
        for(int i=1;i<=n;++i){
            for(int j=0;j<=tune;++j){
                x[x[i].clu].dnb+=x[i].hs[j];
                v[v[i].clu].dnb+=v[i].hs[j];
            }
            ++x[x[i].clu].cnb;
            ++v[v[i].clu].cnb;
        }
        for(int i=1;i<=n;++i){
            x[i].dnb = x[x[i].clu].dnb;
            v[i].dnb = v[v[i].clu].dnb;
            x[i].cnb = x[x[i].clu].cnb;
            v[i].cnb = v[v[i].clu].cnb;

            x[i].hs.push_back(x[x[i].clu].dnb);
            x[i].hs.push_back(x[x[i].clu].clu);

            v[i].hs.push_back(v[v[i].clu].dnb);
            v[i].hs.push_back(v[v[i].clu].clu);
        }
        for(int i=1;i<=n;++i){
            if(x[i].clu==i){
               xabelcir(i);
            }
            if(v[i].clu==i){
               vabelcir(i);
            }
        }


        sort(xx+1,xx+n+1,ptrless);
        sort(vv+1,vv+n+1,ptrless);
       
        ptrdec();
       /* 
            for(int i=1;i<=n;++i){
                cout<<setw(5)<<i<<"   ";
                xx[i]->print();
            }
            for(int i=0;i<s;++i){
                cout<<setw(4)<<y[i][0]<<setw(4)<<y[i][1]<<setw(10)<<y[i][2]<<endl;
                for(int j=y[i][0];j<y[i][1];++j)
                    x[j].fac = y[i][2];
            }
        */
        for(int i=1;i<=n;++i){
            sf[x[i].id] = i;
            is[v[i].id] = i;
        }
        int bg=0,ed=0,sb=0,se=0;
        for(int j=1;j<=n;++j){
            if(ed>j)continue;
            bg = j, ed = j+xx[j]->cnb;
            if(y[sb][0]>=ed)continue;
            while(se<s && y[se][1]<=ed)
                ++se;
            
           //cout<<"solve "<<bg<<' '<<ed<<" "<<sb<<" "<<se<<endl;

            for(int i=bg;i<ed;++i){
                sf[xx[i]->id] = i;
                is[vv[i]->id] = i;
            }

            nxt=se-1,iter=0;
            while(1){
                ++iter;
                if(correct(bg,ed))break;
                nxt = se-1;
                while(nxt>=0&&y[nxt][2]==1){
                    y[nxt][2] = step(y[nxt][1]-y[nxt][0]);
                    next_permutation(xx+y[nxt][0],xx+y[nxt][1],ptrcomp);
                    --nxt;
                }
                if(nxt<sb)break;
                --y[nxt][2];
                next_permutation(xx+y[nxt][0],xx+y[nxt][1],ptrcomp);
                for(int i=bg;i<ed;++i){
                    sf[xx[i]->id]=i;
                }
            }
            sb=se;
        //    cout<<"iter: "<<iter<<endl;
        }   
        //cerr<<"reslut:"<<correct()<<endl;
/*
        if(!correct()){
            for(int i=1;i<=n;++i){
                cout<<setw(5)<<i<<"   ";
                x[i].print();
            }
            cout<<"==============\n";
            for(int i=1;i<=n;++i){
                cout<<setw(5)<<i<<"   ";
                v[i].print();
            }
        }
*/
        for(int i=1;i<=n;++i){
            sf[xx[i]->id] = vv[i]->id;
        }
        for(int i=1;i<=n;++i){
            cout<<sf[i]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
