#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define minl(x,y) (x)->l=(y)->l=(((x)->l<(y)->l)?(x)->l:(y)->l)
#define maxh(x,y) (x)->h=(y)->h=(((x)->h>(y)->h)?(x)->h:(y)->h)
#define findtv (v+e[k].a)->bp==v+e[k].b?v+e[k].a:v+e[k].b
int n,m,pi,*sc,*dy;
long flg=1;
bool *b;
struct edge{
    int a,b,c;
    edge(){};
};
struct vtx{
    vtx(){pr=this;f=0;r=d=1;}
    vtx *pr,*bp;
    vector<vtx*> cd,nb;
    int p,d,f,h,l,r,ll,hh;
};
bool uni(vtx* x,vtx* y){
    while(x->pr != x)
        x = x->pr;
    while(y->pr != y)
        y = y->pr;
    if(x==y)return false;
    if(x->r>y->r)
        y->pr = x;
    else if(x->r<y->r)
        x->pr = y;
    else{
        x->pr = y;
        ++y->r;
    }
    return true;
}
bool compare(edge x,edge y){
    return x.c < y.c;
}
vtx* v,*rt;
int predec(vtx*r){
    ++pi;
    r->l=r->h=r->ll=r->hh=r->p=pi;
    r->f=flg;
    r->d=1;
    for(int i=0;i<r->nb.size();++i)
        if(r->nb[i]->f!=flg){
            r->nb[i]->bp = r;
            r->nb[i]->f = flg;
            r->cd.push_back(r->nb[i]);
        }
    for(int i=0;i<r->cd.size();++i)
        r->d+=predec(r->cd[i]);
    return r->d;
}
void reset(vtx*r){
    if(r->f!=flg){
        r->f = flg;
        r->l = r->ll;
        r->h = r->hh;
    }
}
void cvgext(vtx*r){
    r->f=flg;
    int tmp;
    for(int i=0;i<r->cd.size();++i){
        cvgext(r->cd[i]);
        if(r->hh<r->cd[i]->hh)
            r->hh=r->cd[i]->hh;
        if(r->ll>r->cd[i]->ll)
            r->ll=r->cd[i]->ll;
    }
}
void tell(vtx* tp){
    while(tp->bp!=tp){
        reset(tp->bp);
        if(tp->bp->l>tp->l)tp->bp->l = tp->l;
        if(tp->bp->h<tp->h)tp->bp->h = tp->h;
        tp = tp->bp;
    }
}
int main(){
    cin.tie(NULL);
    ios_base::sync_with_stdio(false); 
    int min,T,bg,ed,nw;
    long long a1,a2;
    edge* e;
    vtx*tp,*tv;
    cin>>T;
    for(int j=0;j<T;++j){
        pi=0,a1=0,a2=0;
        cin>>n>>m;
        e = new edge[m];
        b = new bool[m];
        v = new vtx[n+1];
        for(int i=0;i<m;++i)
            cin>>e[i].a>>e[i].b>>e[i].c,b[i]=0;
        sort(e,e+m,compare);
        for(int i=0;i<m;++i)
            if((b[i]=(uni(v+e[i].a,v+e[i].b)))){
                (v+e[i].a)->nb.push_back(v+e[i].b);
                (v+e[i].b)->nb.push_back(v+e[i].a);
            }
        rt = v+1;
        while(rt->pr!=rt)rt=rt->pr;
        (rt)->bp=rt;
        predec(rt);
        cvgext(rt);
        bool nd = b[0]==0, n2 = b[0]==1;
        for(int i=1,bg=ed=0;i<=m;++flg,++i){
            if(i!=m&&e[i-1].c==e[i].c){
                ed = i;
                nd |= b[i]==0;
                n2 |= b[i]==1;
            }
            else{
                if(bg==ed&&b[bg])a1+=1,a2+=e[bg].c;
                else if(bg!=ed&&!nd){
                    for(int k=bg;k<=ed;++k)
                        a1+=1,a2+=e[bg].c;
                }
                else if(bg!=ed&&n2){
                    for(int k=bg;k<=ed;++k)if(!b[k]){
                        reset(v+e[k].a);
                        reset(v+e[k].b);
                        minl(v+e[k].a,v+e[k].b);
                        maxh(v+e[k].a,v+e[k].b);
                        tell(v+e[k].a);
                        tell(v+e[k].b);
                    }                    
                    for(int k=bg;k<=ed;++k)if(b[k]){
                        tv = findtv;              
                        if(tv->l>=tv->p&&tv->h<tv->d+tv->p)
                            a1+=1,a2+=e[k].c;
                    }
                }
                nd = b[i]==0;
                n2 = b[i]==1;
                bg = ed = i;
            }
        }
        delete[] e;
        delete[] v;
        delete[] b;
        cout<<a1<<" "<<a2<<endl;
    }
    return 0;
}
