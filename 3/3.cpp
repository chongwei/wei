#include<iostream>
#include<algorithm>
using namespace std;
long long n,c,e,p,t,half,i,o,k,rslt,tmp;
long long fac[64],*ap,*cm,*x,d[64];
inline void power(long long k){
    tmp = k;
    rslt = 1;
    for(i=0;i<o;i++){
        if(fac[i])
            rslt=(rslt*tmp)%p;
        tmp=(tmp*tmp)%p;
    }
    ap[k] = rslt;
}
inline bool compare(long long a,long long b){
    k=a+b;
    if(ap[k]==0)power(k);
    return (cm[n+a-b]*ap[k])%p>p/2;
}
int main(){
    long long l,max,j,bnd,tmp,pm[1000];
    for(k=2,n=0;n<1000;k++){
        for(j=0;j<n;j++)
            if(k%pm[j]==0)break;
        if(j==n)pm[n++]=k;
    }
    cin>>t;
    for(max=0,l=0;l/4<t;l+=4){
        cin>>d[l]>>d[l+1]>>d[l+2]>>d[l+3];
        max=(d[l]>max?d[l]:max);
    }
    x  = new long long[max+1];
    ap = new long long[max*2+1];
    cm = new long long[max*2+1];
    for(l=0;l/4<t;l+=4){
        c=d[l+1],p=d[l+3],e=d[l+2]%(p-1),n=d[l];
        for(o=0;e>0;e/=2)
            fac[o++]=e%2;
        for(k=2;k<n*2;k++)
            ap[k] = 0;
        for(tmp=k=0;tmp*tmp<n||k<1000;k++){
            tmp = pm[k];
            bnd = n*2/tmp;
            power(tmp);
            for(j=2;j<bnd;j++)
                if(ap[j]!=0)
                    ap[j*tmp]=(ap[j]*ap[tmp])%p;
        }
        for(k=-n;k<n;k++)
            cm[n+k]=((c*k)%p+p)%p;
        for(k=0;k<n;k++) 
            x[k]=k+1;
        sort(x,x+n,compare);
        for(k=0;k<n;k++) 
            cout<<x[k]<<" ";
        cout<<endl;
    }   
    return 0;
}
