#include<iostream>
#include<string>
using namespace std; 
string small(int c,string r = ""){
    if(c%9) r += (c%9)+'0';
    for(int i=0;i<c/9;i++)r+='9';
    return r;
}
string conc(string a,string b){
    int i,sa=a.size(),sb=b.size(),j;
    for(int i=sa-sb,j=0;i<sa;i++,j++)a[i]=b[j];
    if (a[0] == '0')return a.substr(1);
    return a;
}
string next(int n, string& od){
    if(small(n).size()>od.size())return small(n);
    int r = 0,i,lnn=0,j,c=n,t=0;
    string nw="0"+od;
    for(i=0;i<od.size()&&c>0;i++){
        if((c/9+(c%9>0))>(od.size()-i))break;
        c -= (od[i]-'0');
        if(i>0&&od[i-1]!='9')lnn = i;
    }
    if(c<=0){
        nw[lnn]+=1;
        for(j=0;j<=lnn;j++) t += (nw[j]-'0');
        for(j=lnn+1;j<nw.size();j++) nw[j]='0';
        return conc(nw,small(n-t));
    }
    else return conc(nw,small(c+(od[i-1]-'0')));
}
int main(){
    string x="0";
    int i,n,j,T,c[1024],k;
    cin>>T;
    for(i=0;i<T;i++,x="0"){
        cin>>n;
        for(j=0;j<n;j++)cin>>c[j];
        for(j=0;j<n;j++){
            x = next(c[j],x);
            cout<<x<<((j!=n-1)?" ":"\n");
        }
    }
    return 0;
}
